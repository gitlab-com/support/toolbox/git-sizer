# build stage
FROM golang:alpine AS build-env
RUN apk add --no-cache git
RUN go install github.com/github/git-sizer@latest

FROM alpine:latest
RUN apk add --no-cache git
COPY --from=build-env /go/bin/git-sizer /usr/bin/git-sizer
CMD git-sizer
