# git-sizer

## What is this?

An easy way to run [git-sizer](https://github.com/github/git-sizer) to compute size metrics for a Git repository in the context of a GitLab CI job.

### How do I use this?

Have a large repo? Can't figure out what's taking up so much space? Looking for detailed size metrics?

Add one of the following to a `.gitlab-ci.yml` file in the root of your repository:

```yaml
include:
  remote: 'https://gitlab.com/gitlab-com/support/toolbox/git-sizer/-/raw/main/git-sizer.yml'
```

OR

```yaml
variables:
  GIT_STRATEGY: clone
  GIT_DEPTH: 0

git-sizer:
  image: "registry.gitlab.com/gitlab-com/support/toolbox/git-sizer:latest"
  script:
    - git-sizer --verbose --show-refs | tee -a size-report.txt
  artifacts:
    paths:
      - size-report.txt
```
